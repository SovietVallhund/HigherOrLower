﻿namespace HigherOrLower
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ansBox = new System.Windows.Forms.TextBox();
            this.guessButton = new System.Windows.Forms.Button();
            this.watermark = new System.Windows.Forms.Label();
            this.answerLbl = new System.Windows.Forms.Label();
            this.livesLbl = new System.Windows.Forms.Label();
            this.ansLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to Higher or Lower!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Please input a value 1 - 20";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Your guess:";
            // 
            // ansBox
            // 
            this.ansBox.Location = new System.Drawing.Point(16, 101);
            this.ansBox.Name = "ansBox";
            this.ansBox.Size = new System.Drawing.Size(100, 20);
            this.ansBox.TabIndex = 4;
            this.ansBox.TextChanged += new System.EventHandler(this.ansBox_TextChanged);
            // 
            // guessButton
            // 
            this.guessButton.Location = new System.Drawing.Point(123, 93);
            this.guessButton.Name = "guessButton";
            this.guessButton.Size = new System.Drawing.Size(80, 34);
            this.guessButton.TabIndex = 6;
            this.guessButton.Text = "Guess";
            this.guessButton.UseVisualStyleBackColor = true;
            this.guessButton.Click += new System.EventHandler(this.guessButton_Click);
            // 
            // watermark
            // 
            this.watermark.AutoSize = true;
            this.watermark.ForeColor = System.Drawing.Color.Silver;
            this.watermark.Location = new System.Drawing.Point(66, 200);
            this.watermark.Name = "watermark";
            this.watermark.Size = new System.Drawing.Size(147, 13);
            this.watermark.TabIndex = 7;
            this.watermark.Text = "Developed by SovietVallhund\r\n";
            // 
            // answerLbl
            // 
            this.answerLbl.AutoSize = true;
            this.answerLbl.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerLbl.ForeColor = System.Drawing.Color.Red;
            this.answerLbl.Location = new System.Drawing.Point(99, 56);
            this.answerLbl.Name = "answerLbl";
            this.answerLbl.Size = new System.Drawing.Size(0, 21);
            this.answerLbl.TabIndex = 8;
            // 
            // livesLbl
            // 
            this.livesLbl.AutoSize = true;
            this.livesLbl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.livesLbl.ForeColor = System.Drawing.Color.Black;
            this.livesLbl.Location = new System.Drawing.Point(16, 148);
            this.livesLbl.Name = "livesLbl";
            this.livesLbl.Size = new System.Drawing.Size(65, 21);
            this.livesLbl.TabIndex = 9;
            this.livesLbl.Text = "Lives: 5";
            // 
            // ansLbl
            // 
            this.ansLbl.AutoSize = true;
            this.ansLbl.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ansLbl.Location = new System.Drawing.Point(118, 47);
            this.ansLbl.MaximumSize = new System.Drawing.Size(150, 30);
            this.ansLbl.Name = "ansLbl";
            this.ansLbl.Size = new System.Drawing.Size(37, 30);
            this.ansLbl.TabIndex = 11;
            this.ansLbl.Text = "---";
            this.ansLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 222);
            this.Controls.Add(this.ansLbl);
            this.Controls.Add(this.livesLbl);
            this.Controls.Add(this.answerLbl);
            this.Controls.Add(this.watermark);
            this.Controls.Add(this.guessButton);
            this.Controls.Add(this.ansBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Higher or Lower";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ansBox;
        private System.Windows.Forms.Button guessButton;
        private System.Windows.Forms.Label watermark;
        private System.Windows.Forms.Label answerLbl;
        private System.Windows.Forms.Label livesLbl;
        private System.Windows.Forms.Label ansLbl;
    }
}

